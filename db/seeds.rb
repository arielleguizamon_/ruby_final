# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

patient1 = Patient.create(:name => "pepe", :surname => "pepito",:dni => 12345,:age => 20,:mail => "pepe@pepito.com")
patient2 = Patient.create(:name => "juan", :surname => "lopez",:dni => 54345,:age => 80,:mail => "juan@mail.com")
patient3 = Patient.create(:name => "ernesto", :surname => "leon",:dni => 9999,:age => 30,:mail => "erensto@mail.com")
patient4 = Patient.create(:name => "juana", :surname => "apellido",:dni => 1111,:age => 45,:mail => "juana@juana.com")

