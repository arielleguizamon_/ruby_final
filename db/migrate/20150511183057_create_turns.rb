class CreateTurns < ActiveRecord::Migration
  def change
    create_table :turns do |t|
      t.datetime :date

      t.timestamps null: false
    end
  end
end
