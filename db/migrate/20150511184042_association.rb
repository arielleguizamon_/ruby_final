class Association < ActiveRecord::Migration
  def change
    add_reference :patients, :turn, index: true, foreign_key: true
  end
end
