class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.integer :number

      t.timestamps null: false
    end
  end
end
