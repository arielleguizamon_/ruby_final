class Patient < ActiveRecord::Base
  has_many :turns
  validates :mail, uniqueness: true
  validates :dni, uniqueness: true
end
